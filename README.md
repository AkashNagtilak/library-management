
# Library Management System

Library Management System.



## Installation

Step by Step Installation this project.

```bash
  python -m venv <venv_name>

  source <venv_name>/bin/activate

  git clone https://gitlab.com/AkashNagtilak/library-management.git

  pip install -r requirements.txt

  create database 

  mysql -u <username> -p 
  password <password>

  create database <database name>
  
  python manage.py makemigrations
  python manage.py migrate

  python manage.py createsuperuser
```
Finally done our setup.






    
