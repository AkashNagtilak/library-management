from django.contrib import admin

from django.apps.registry import apps

User = apps.get_model('user', 'User')
UserProfile = apps.get_model('user', 'UserProfile')
# Register your models here.
admin.site.register(User)
admin.site.register(UserProfile)