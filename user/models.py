# django imports
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings


class User(AbstractUser):

    """
    Django user model.
    Model to store system users.
    """

    middle_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(unique=True)

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        # default method to return object display name
        try:
            display_name = self.username
            if self.first_name and self.last_name:
                display_name = '%s %s - %s' % (self.first_name, self.last_name, self.email)
            if self.first_name:
                display_name = '%s - %s' % (self.first_name, self.email)
            return display_name
        except Exception:
            return self.username


class UserProfile(models.Model):

    """
    Model to store user profile details
    """

    GENDERS = (('Male', 'Male'), ('Female', 'Female'),)

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_profile')

    profile_image = models.FileField(blank=True, null=True, upload_to='user/profile_images/')
    contact_number = models.CharField(max_length=100, blank=True, null=True)

    gender = models.CharField(max_length=100, blank=True, null=True, choices=GENDERS)
    dob = models.DateField(blank=True, null=True)

    address_line_1 = models.CharField(max_length=100, blank=True, null=True)
    address_line_2 = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        # default method
        return self.user.username
