# django imports
from django.urls import path
from django.contrib.auth.views import LogoutView, PasswordResetView, PasswordResetDoneView, PasswordChangeView
# internal imports
from . import views as user_views

urlpatterns = [
    path('login/', user_views.CustomLoginView.as_view(), name='user-login'),
    path('dashboard/', user_views.DashboardTestTemplate.as_view() ,name='dashboard'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('password_reset/', PasswordResetView.as_view(), name="user-password_reset"),
    path('password_reset/done/', PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('password_change/', PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', user_views.CustomPasswordChangeDoneView.as_view(), name='password_change_done'),
    path('register/', user_views.RegisterView.as_view(), name='user-register'),
]



