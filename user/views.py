from django.contrib.auth.views import LoginView, PasswordChangeView
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from django.views import generic
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.apps.registry import apps
from .forms import CustomUserChangeForm

User = apps.get_model('user', 'User')
UserProfile = apps.get_model('user', 'UserProfile')

# Create your views here.
class CustomLoginView(LoginView):

    def form_valid(self, form):
        """Security check complete. Log the user in."""
        auth_login(self.request, form.get_user())

        if not self.request.POST.get('remember-me'):
            self.request.session.set_expiry(0)

        return HttpResponseRedirect(self.get_success_url())

class CustomPasswordChangeDoneView(PasswordChangeView):
    success_url = reverse_lazy('dashboard')


class RegisterView(generic.FormView):
    template_name = 'registration/register.html'
    form_class = CustomUserChangeForm
    success_url = '/dashboard/'


    def form_valid(self, form):
        base = form.save()
        UserProfile.objects.create(user=base, profile_image=form.cleaned_data.get('profile_image'))
        return HttpResponseRedirect(self.get_success_url())


@method_decorator(login_required(login_url='/login/'), name='dispatch')
class DashboardTestTemplate(generic.TemplateView):
    template_name = 'include/creative/dashboard.html'