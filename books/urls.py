from django.urls import path
from . import views as books

urlpatterns = [
    path('', books.StudentView.as_view(), name= "student-view"),
    path('book-list/', books.BookListView.as_view(), name="book-list"),
    path('book-create/', books.BookCreateView.as_view(), name="book-create"),
    path('book-update/<int:pk>/', books.BookUpdateView.as_view(), name= "book-update"),
    path('book-delete/<int:pk>/', books.BookDeleteView.as_view(), name= "book-delete"),
]
