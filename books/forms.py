from django import forms
from django.apps.registry import apps


Book = apps.get_model('books', 'Book')

class BookCreateForm(forms.ModelForm):

    class Meta:
        model = Book
        fields = ("name", "isbn", "author", "category","discription")

    def __init__(self, *args, **kwargs):
        super(BookCreateForm, self).__init__(*args, **kwargs)

        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['name'].required = True

        self.fields['isbn'].widget.attrs['class'] = 'form-control'
        self.fields['isbn'].required = True

        self.fields['author'].widget.attrs['class'] = 'form-control'
        self.fields['author'].required = True

        self.fields['category'].widget.attrs['class'] = 'form-control'
        self.fields['category'].required = True

        self.fields['discription'].widget.attrs['class'] = 'form-control'
        self.fields['discription'].required = True

        self.fields['name'].widget.attrs['placeholder'] = 'Book Name'
        self.fields['isbn'].widget.attrs['placeholder'] = 'ISBN Number'
        self.fields['author'].widget.attrs['placeholder'] = 'Author Name'
        self.fields['discription'].widget.attrs['placeholder'] = 'Discription'

