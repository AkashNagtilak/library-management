# Register your models here.
from django.contrib import admin

from django.apps.registry import apps

Book = apps.get_model('books', 'Book')
# Register your models here.
admin.site.register(Book)
