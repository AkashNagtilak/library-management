from django.shortcuts import render
from django.views import generic
from django.apps.registry import apps
from django.contrib.auth.mixins import LoginRequiredMixin


from .forms import BookCreateForm

Book = apps.get_model('books', 'Book')

class StudentView(generic.View):
    """
        Class for Student view.
    """
    model = Book
    template_name = "books/student_view.html"
    def get(self, request, *args, **kwargs):
        books = self.model.objects.all()
        context = {
            "books" : books
        }
        return render(request, self.template_name, context=context)

# Create your views here.
class BookListView(LoginRequiredMixin, generic.ListView):
    """
        Class for book list view
    """
    model = Book
    template_name = "books/book_list.html"
    context_object_name = 'book_list'


class BookCreateView(LoginRequiredMixin, generic.CreateView):   
    """
        Class for create book.
    """
    model = Book
    template_name = "books/book_create.html"
    form_class = BookCreateForm
    success_url = '/book-list/'


class BookUpdateView(LoginRequiredMixin, generic.UpdateView):
    """
        Class for update view.
    """
    model = Book
    template_name = "books/book_update.html"
    form_class = BookCreateForm
    success_url = '/book-list/'


class BookDeleteView(LoginRequiredMixin, generic.DeleteView):
    """
        Class for delete view.
    """
    model = Book
    template_name = "books/book_delete.html"
    success_url = '/book-list/'